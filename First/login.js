function solve(input){
    let  pass, wrongPass, count;
    count = 0;
    wrongPass = 0;
    let firstName = input.shift();
    pass = firstName.split("").reverse().join("");
    while(wrongPass != pass){
        count++;
        wrongPass = input.shift();
        if(count > 3 && wrongPass !== pass){
            console.log("User " + firstName + " blocked!");
            break;
        }

        if(wrongPass !== pass){
            console.log('Incorrect password. Try again.');
        }else{
            console.log("User " + firstName + " logged in.")
        }

    }
}

solve(["ho", 2, 3, 4,   "oh"])