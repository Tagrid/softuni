function solve(grams){
    let gramPerDay, days, money, bitcoins;
    days = 1;
    bitcoins = 0;
    money = 0;
    firstDayOfPurshase = 0
    let loops = grams.length

    while(days <= loops) {
        gramPerDay = grams.shift();
    
        if(days % 3 === 0){
            gramPerDay = gramPerDay * 0.7;
        }

        money += gramPerDay * 67.51;
        
        while(money >= 11949.16){
            money -= 11949.16;
            bitcoins++;
            if(firstDayOfPurshase === 0){
                firstDayOfPurshase = days;
            }
        }

        days++;
    }

console.log("Bought bitcoins: " + bitcoins);
if(bitcoins > 0){
    console.log("Day of the first purchased bitcoin: " + firstDayOfPurshase);
}
console.log("Left money: " + money.toFixed(2) + " lv.")
}

solve([100, 200, 300]);