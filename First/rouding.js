function solve(num, percision){
    if(percision > 15){
        percision = 15;
    }

    num = num.toFixed(percision)
    console.log(parseFloat(num));
}

solve(12.1235635043200000000000, 8)